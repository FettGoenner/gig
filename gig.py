# GETINTOGIG WEBSITE
from flask import Flask, render_template, request, redirect
# Self-made modules
from plot import plot

app = Flask(__name__)

# PREVENT THE BROWSER FROM CACHING
# this is good for developing. bad for deploying
@app.after_request
def set_response_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response


# Root Page
@app.route('/')
def index():
    return render_template('index.html')

# ZEROTONIN BAND INFO
# @app.route('/zerotonin')
# def zerotonin():
#     return render_template('zerotonin.html')

# GREY WATER LIFTING MACHINE AUDIO FILES
@app.route('/gwlm')
def gwlm():
    return render_template('gwlm.html')

# DOPPELKOPF
@app.route('/doko', methods=["GET", "POST"])
def dict():
    msg = ""
    if request.method == "POST":
        passwd = request.form["pass"]
        if passwd == "Karlchen":
            game = request.form["game"]
            # Make new plot
            plot(app.root_path)
        else:
            msg = "Falsches Passwort!"
        game = request.form["game"]
    return render_template('doko.html', msg=msg)

if __name__ == "__main__":
    app.run()
