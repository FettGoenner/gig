gruv = {
    "bg"    :   '#282828',
    "fg"    :   '#ebdbb2',
    "gray"  :   '#928374',
    "grayl" :   '#a89984',
    "fg2"   :   '#d5c4a1',
    # colors
    "red"   :   '#cc241d',
    "green" :   '#98971a',
    "yellow":   '#d79921',
    "blue"  :   '#458588',
    "purple":   '#b16286',
    "aqua"  :   '#689d6a',
    "orange":   '#d65d0e',
    # lighter version colors
    "redl"   :   '#fb4934',
    "greenl" :   '#b8bb26',
    "yellowl":   '#fabd2f',
    "bluel"  :   '#83a598',
    "purplel":   '#d3869b',
    "aqual"  :   '#8ec07c',
    "orangel":   '#fe8019',
}
