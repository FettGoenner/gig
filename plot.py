from gruvbox import gruv
import matplotlib.pyplot as plt
import numpy as np
import csv

color = gruv["bluel"]

def plot(path):
    """
    Plotting data from file path/total.csv and save plot
    """
    # Load player names
    with open(path + "/total.csv", newline='') as f:
        reader = csv.reader(f)
        names = next(reader)  # gets the first line
    # Load matrix
    total = np.loadtxt(open(path + "/total.csv"), delimiter=",",skiprows=1)
    # get dimensions
    m,n = total.shape
    # setting figure properties
    fig = plt.figure(figsize=(10,5))
    ax = fig.add_subplot(111)
    ax.yaxis.set_label_position("right")
    ax.yaxis.tick_right()
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)
    ax.tick_params(colors=color)
    # plot each players data
    for i in range(n):
        plt.plot(range(1,m+1),
                 total[:,i],
                 color=list(gruv.values())[(5 + i)%14],
                 label=names[i])
    # add grid and legend
    plt.grid(color=color)
    fig.legend(framealpha=0,
               loc="upper center",
               labelcolor=gruv["fg"],
               ncol=10)
    # save figure as png
    fig.savefig(path + "/static/img/doko.png",
                edgecolor=gruv["fg"],
                bbox_inches='tight',
                transparent=True)
